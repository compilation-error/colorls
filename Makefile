clean:
	rm -rf build/
	rm -rf dist/
	rm -rf color_ls.egg-info

build:
	uv build

test_publish:
	twine upload -r testpypi dist/* --config-file $(XDG_CONFIG_HOME)/pypirc --verbose

publish:
	twine upload dist/* --config-file $(XDG_CONFIG_HOME)/pypirc --verbose

all:
	clean build publish

test_all:
	clean build test_publish
